import csv
import os
from urllib import quote_plus

from org.jsoup import Jsoup
from org.jsoup.nodes import Document, Element
from org.jsoup.select import Elements

resource_directory = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../resources"))


def get_page_sample_text():
    return get_entries_as_list("pages/pagesSampleData.csv")


def get_space_key():
    space_keys = get_entries_as_list("spaces/spaceKeys.csv")
    return space_keys[0]["space_key"] if len(space_keys) > 0 else "ds"


def get_pages_by_title():
    return get_entries_as_list("pages/pagesByTitle.csv")


def get_pages_not_found():
    return get_entries_as_list("pages/pagesNotFound.csv")


def get_space_actions():
    return get_entries_as_list("spaces/spaces.csv")


def get_rss_feed_types():
    return get_entries_as_list("spaces/rss.csv")


def get_oc_pages():
    return get_entries_as_list("pages/pagesOC.csv")


def get_search_keywords():
    return get_entries_as_list("search/keywords.csv")


def get_labels():
    return get_entries_as_list("search/labels.csv")


def get_quick_nav_keywords():
    return get_entries_as_list("search/quicknavkeywords.csv")


def get_pages_to_comment():
    return get_entries_as_list("pages/pagesToComment.csv")


def get_pages_to_edit():
    return get_entries_as_list("pages/pagesToEdit.csv")


def get_sample_comments():
    return get_entries_as_list("comments/comments.csv")


def get_meta_attribute(feed_text, attribute):
    doc = Jsoup.parse(feed_text)
    element = doc.select("meta[name='%s']" % attribute).first()
    return element.attr("content")


def get_input_value(feed_text, input_id):
    doc = Jsoup.parse(feed_text)
    element = doc.select("input#%s" % input_id).first()
    return element.attr("value")


def get_content_by_id(feed_text, element_id):
    doc = Jsoup.parse(feed_text)
    element = doc.select("#%s" % element_id).first()
    return element.html()


def url_encode(string):
    return quote_plus(string)


def get_entries_as_list(file_name):
    csv_file_path = os.path.abspath(os.path.join(resource_directory, file_name))
    with open(csv_file_path) as csv_file:
        entries = csv.DictReader(csv_file)
        return list(entries)



