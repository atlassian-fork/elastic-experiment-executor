# Configuring Collectd

The Performance Testing Framework uses the collectd service to collect system-level and application-level metrics. The
config files supplied here are used to configure collectd when provisioning worker and instance clusters in AWS. However,
if you are using your own infrastructure, you may need to make some adjustments depending on your Linux 
distribution / collectd installation. Some common examples are noted here.

## What to watch out for

### Collectd built-in defaults

Depending on your Linux distribution, collectd has a number of default locations for the location of the config file, 
the plugin directory, the data directory, etc. The sample configuration files included here set those locations explicitly.
If your collectd installation has different defaults, modify the file accordingly.

**However**, please do not modify the data directory paths for either the `rrdtool` or `csv` plugins. The Performance Testing
Framework uses those paths in [`Gather.py`](../Gather.py) to gather data after experiment runs. if you do need to change them,
you'll need to modify `Gather.py` accordingly.

### Collectd Interface plugin

The sample configuration sets up the collectd-interface plugin to monitor `eth0`. However, some newer Linux distributions
may use alternative naming schemes. Currently, the Performance Testing Framework supports monitoring `eth0` and network
interfaces with the `enp#s#` format. If you'd like to use a different scheme, you'll need to modify 
[`AnalyzeCollectd.py`](../analysis/gnuplot/AnalyzeCollectd.py) in order to produce graphs that include the relevant metrics.