#!/usr/bin/env python

import json
import click
import requests
import logging
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from common.E3 import e3
from provisioning.confluence.UserCreatorHelper import UserCreatorHelper


class AddUsers:
    def __init__(self, count, username, password, base_url, keyfile):
        self._log = logging.getLogger('provision')
        self.count = int(count)
        self.auth = (username, password)
        self.base_url = base_url
        self.keyfile = keyfile

    def for_bitbucket(self):
        key_pairs = json.load(open(self.keyfile))
        available_keys = key_pairs['count']
        if int(available_keys) < self.count:
            raise Exception("Not enough keys. Specified %d new users, %d keys available" % (self.count, available_keys))

        for i in range(self.count):
            add_user_url = 'http://%(base_url)s/rest/api/1.0/admin/users?name=%(name)s&password=%(password)s&' \
                           'displayName=%(display_name)s&emailAddress=%(email_address)s' \
                           '&true' % {'base_url': self.base_url,
                                      'name': 'user_%s' % i,
                                      'password': 'password_%s' % i,
                                      'email_address': 'foo_%s@bar.com' % i,
                                      'display_name': 'display_name_%s' % i}
            self._log.debug(add_user_url)
            response = requests.post(
                add_user_url, auth=self.auth, headers={"Content-Type": "application/json"})
            self._log.info("Adding user #%d using url %s response { status code:%s, text:%s }" % (i, add_user_url,
                                                                                                  response.status_code,
                                                                                                  response.text))

            add_key_url = 'http://%(base_url)s/rest/ssh/1.0/keys?user=%(username)s' % {'base_url': self.base_url,
                                                                                       'username': 'user_%s' % i}

            public_key = key_pairs['key_pairs'][i]['public_key']
            self._log.debug(public_key)
            response = requests.post(add_key_url,
                                     auth=self.auth,
                                     data=json.dumps({'text': public_key}),
                                     headers={"Content-Type": "application/json"})

            self._log.debug(add_key_url)
            self._log.debug(response.status_code)
            self._log.debug(response.text)
            self._log.info("Finished generating adding %d users with key pairs to %s" % (self.count, self.base_url))

    def for_confluence(self):
        helper = UserCreatorHelper(total_user=self.count, base_url=self.base_url, auth_user=self.auth)
        helper.prepare_test_users()


def prompt_keyfile(ctx, param, value):
    if 'bitbucket' in str.lower(str(ctx.params['product'])) and value is None:
        value = raw_input('Key file path: ')

    return str(value)


@click.command()
@click.option('--product', help="Product under test -- either Confluence or Bitbucket", required=True)
@click.option('--url', help="Base url of product instance", required=True, prompt='Instance base url')
@click.option('--username', help="Admin username", required=True, prompt='Admin username')
@click.option('--password', help="Admin password", required=True, prompt='Admin password', hide_input=True, confirmation_prompt=True)
@click.option('--users', help="Number of users to add", type=click.INT, default=100, prompt=True)
@click.option('--keyfile', help="Path to file with user keys", callback=prompt_keyfile)
def command(product, url, username, password, users, keyfile):
    add_users = AddUsers(users, username, password, url, keyfile)
    if 'confluence' in str.lower(str(product)):
        add_users.for_confluence()
    else:
        add_users.for_bitbucket()

if __name__ == '__main__':
    e3.setup_logging()
    command()
