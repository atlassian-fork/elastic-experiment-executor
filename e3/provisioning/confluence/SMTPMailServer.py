import requests


class SMTPMailServer:
    SMTP_ACTION_URL = "admin/mail/createmailserver.action?protocol=smtp"

    def __init__(self):
        self._type = None
        self._server_info = {'name': "E3 Test Mail Server", 'emailAddress': "ad@minnie.com"}    # default values

    def setup_mail_trap(self, api_token):
        auth = {'api_token': api_token}
        response = requests.get("https://mailtrap.io/api/v1/inboxes", params=auth)
        info = response.json()[0]

        self._server_info['hostname'] = str(info['domain'])
        self._server_info['userName'] = str(info['username'])
        self._server_info['password'] = str(info['password'])
        self._server_info['port'] = str(info['smtp_ports'][0])

        self._type = "mail_trap"

    def setup_custom(self, filepath):
        with open(filepath) as f:
            for line in f:
                (key, val) = line.split('=')
                val = val.strip()
                if val != '':
                    self._server_info[key.strip()] = val

        self._type = "custom"

    def get_server_info(self):
        return self._server_info
