import csv
import os
import logging

from common.E3 import e3
from ConfluenceSpaceUtils import *
from UPMHelper import disable_plugin
from SMTPMailServer import SMTPMailServer
from ConfluenceSetupWizard import BundleSelectionPage, ConfluenceInstance, ConfluenceSecuritySettingsPage, \
    ConfluenceMailServerSetupPage


class SetupHelper:
    # properties
    RESOURCE_DIR = "confluence.data.directory"
    LICENSE_KEY = "confluence.license.key"
    INSTANCES_COUNT = "confluence.number.of.instances"
    TOTAL_USER_COUNT = "confluence.number.of.users"
    MAILTRAP_TOKEN = "mailtrap.api.token"

    # resource files
    SPACE_IMPORT = "space-import.xml.zip"
    MAIL_SERVER_PROPERTIES = "mail-server.properties"

    def __init__(self):
        self._log = logging.getLogger('provision')

    @staticmethod
    def get_space_key():
        space_keys_file = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                       "../../execution/grinder/confluence/resources",
                                                       "spaces/spaceKeys.csv"))
        with open(space_keys_file) as csv_file:
            entries = list(csv.DictReader(csv_file))
            return entries[0]["space_key"] if len(entries) > 0 else "ds"

    def do_setup(self, conf_instance):
        select_bundles = BundleSelectionPage(conf_instance).visit()
        license_page = select_bundles.go_next()
        self._log.info("--------------------Selecting no bundles--------------------------------")
        load_content_page = license_page.fill_license().go_next()
        self._log.info("--------------------Configuring license---------------------------------")
        user_mgmt_page = load_content_page.with_empty_site()
        self._log.info("--------------------Loading empty site----------------------------------")
        setup_admin_page = user_mgmt_page.with_confluence_manage_users()
        self._log.info("--------------------Configuring internal user management----------------")
        finish_setup_page = setup_admin_page.fill_admin_info().go_next()
        self._log.info("--------------------Adding admin account--------------------------------")
        further_settings_page = finish_setup_page.go_next()
        self._log.info("--------------------Confluence Further Settings-------------------------")
        further_settings_page.login_web_sudo().enable_xml_rpc().submit()
        self._log.info("--------------------Confluence Security Settings------------------------")
        security_settings_page = ConfluenceSecuritySettingsPage(conf_instance).visit()
        security_settings_page.disable_web_sudo().submit()
        self._log.info("--------------------Disabling WebSudo-----------------------------------")
        disable_plugin(conf_instance.base_url, "com.atlassian.confluence.plugins.confluence-onboarding")
        self._log.info("--------------------Disabling Onboarding--------------------------------")

        # only import if user input data-dir property
        if self.RESOURCE_DIR in conf_instance.properties:
            (confluence_xmlrpc, rpc_token) = authenticate_rpc(conf_instance.base_url)
            filepath = os.path.join(e3.get_e3_home(), conf_instance.properties[self.RESOURCE_DIR], self.SPACE_IMPORT)
            imported = import_space(confluence_xmlrpc, rpc_token, filepath)
            self._log.info("--------------------Importing space-------------------------------------")

        if self.MAILTRAP_TOKEN in conf_instance.properties or self.RESOURCE_DIR in conf_instance.properties:
            mail_server = SMTPMailServer()

            if self.MAILTRAP_TOKEN in conf_instance.properties:
                mail_server.setup_mail_trap(conf_instance.properties[self.MAILTRAP_TOKEN])
            else:
                mail_server.setup_custom(os.path.join(e3.get_e3_home(), conf_instance.properties[self.RESOURCE_DIR], self.MAIL_SERVER_PROPERTIES))

            setup_mail_server_page = ConfluenceMailServerSetupPage(conf_instance, path=mail_server.SMTP_ACTION_URL).visit()
            setup_mail_server_page.fill_smtp_mail_server_info(mail_server.get_server_info()).submit()
            self._log.info("--------------------Setting up mail server-------------------------------")


if __name__ == '__main__':
    confluence_instance = ConfluenceInstance("http://localhost:8080/confluence/", "confluence.license.key=<license string here>")
    setup_helper = SetupHelper()
    setup_helper.do_setup(conf_instance=confluence_instance)
