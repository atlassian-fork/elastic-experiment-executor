import random
import time

from HTTPClient import NVPair
from confluence.common.helper.ResourceUtils import *
from net.grinder.script.Grinder import grinder
from confluence.common.MacroHelper import *

from Tools import *


def is_running_special_action(test_script, run_num):
    return not run_num % 10


def visit_dashboard(test_script):
    test_script.http("GET", "/dashboard.action#all-updates")

    test_script.rest("GET", "/rest/mywork/latest/status/notification/count")

    test_script.rest("GET", "/rest/recentlyviewed/1.0/recent?limit=8")

    test_script.rest("GET", "/rest/api/content/search?expand=%s&cql=%s" %
                     ('container,metadata.currentuser.lastmodified',
                      'type in (page,blogpost) and id in recentlyModifiedPagesAndBlogPostsByUser(currentUser(), 0, 20)'
                      ))

    test_script.rest("GET", "/rest/experimental/search?cql=%s&expand=%s&limit=%s" %
                     ('type=space and space.type=favourite order by favourite desc',
                      'space.icon',
                      '100'
                      ))


def visit_page_history(test_script, page_id):
    return test_script.http("GET", "/pages/viewpreviousversions.action?pageId=" + page_id)


def visit_page_attachments(test_script, page_id):
    return test_script.http("GET", "/pages/viewpageattachments.action?pageId=" + page_id)


def upload_image_into_page(test_script, page_id, space_key, atl_token):
    rand_str = ''.join([random.SystemRandom().choice(string.lowercase + string.digits) for i in xrange(10)]) + "_" + atl_token
    upload_url = "/plugins/drag-and-drop/upload.action?" \
                 "pageId=%s&" \
                 "filename=confluence_logo_%s.png&" \
                 "size=1705&" \
                 "minorEdit=false&" \
                 "spaceKey=%s&" \
                 "mimeType=image/png&" \
                 "atl_token=%s&" \
                 "name=confluence_logo_%s.png" % (page_id, rand_str, space_key, atl_token, rand_str)
    real_url = "%s%s" % (test_script.test_data.base_url, upload_url)
    test_script.request.POST(real_url,
                             "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A\x00\x00\x00\x0D\x49\x48\x44\x52"
                             "\x00\x00\x00\x81\x00\x00\x00\x81\x08\x03\x00\x00\x00\xD0\x7E\x29"
                             "\x62\x00\x00\x00\x75\x50\x4C\x54\x45\xFF\xFF\xFF\x21\x4F\x81\x1B"
                             "\x4C\x7F\x00\x3F\x78\x17\x4A\x7E\x00\x43\x7A\x47\x69\x90\xF6\xF7"
                             "\xF9\x00\x3B\x76\x47\x67\x90\x00\x46\x7E\xFA\xFC\xFD\x13\x48\x7D"
                             "\x00\x41\x79\x3B\x60\x8C\x55\x72\x97\x86\x9A\xB3\x00\x32\x72\xAC"
                             "\xB9\xCA\x62\x7E\xA0\x6A\x85\xA5\xC3\xCD\xD8\xB9\xC2\xD1\xF3\xF2"
                             "\xF2\xC1\xC7\xD2\x00\x37\x74\xE5\xE9\xEE\xDB\xDF\xE6\xCB\xD4\xDE"
                             "\x2A\x55\x84\x9D\xAE\xC1\xEC\xF0\xF3\x95\xA5\xBA\x76\x8D\xA9\x7F"
                             "\x95\xB2\x00\x29\x6E\x00\x01\x64\x00\x1D\x67\x8D\x9B\xAF\x68\xA8"
                             "\x8F\x2B\x00\x00\x05\xEF\x49\x44\x41\x54\x78\x9C\xED\x5A\xD9\x76"
                             "\xAC\x2A\x10\x15\x04\x45\x45\x9C\x5A\x4F\x14\xE7\x9C\x9C\xFF\xFF"
                             "\xC4\x5B\x05\x98\x74\xE6\x3C\xB4\x9D\xB5\xEE\x62\xE7\x41\x50\x81"
                             "\xDD\x45\x51\x93\x09\x02\x0F\x0F\x0F\x0F\x0F\x0F\x0F\x0F\x0F\x0F"
                             "\x0F\x0F\x0F\x0F\x8F\x9B\x22\xFE\xE6\x79\x76\x36\x81\x21\xE4\xFB"
                             "\xF8\xE9\x2A\x43\x1D\x3D\x75\x27\x33\x58\x26\x2A\xE5\x74\xA9\xFF"
                             "\xBC\x7F\xD4\x96\x09\x4B\x59\x5A\x9F\xCC\x20\x48\x28\x21\x54\x8A"
                             "\x0B\xB6\xB3\x71\xD4\x5A\x8F\xED\x82\x9D\xAE\xE2\xF0\x88\xC8\xF1"
                             "\x6C\x06\x7B\x4A\x10\xB9\x86\xF6\xF8\xD8\x03\xC4\xE3\x8E\x6C\x0A"
                             "\x6A\x1E\x4C\xCB\xD9\x0C\xDA\xD0\x2C\x24\x1B\x68\xC7\x89\x59\x35"
                             "\xC4\x9F\xDD\x09\x7B\xFF\x72\x36\x81\x63\x55\x92\x76\xC7\xB2\x66"
                             "\x51\xD0\x0F\x73\x9B\xB7\xA7\x33\x08\x66\x2B\x04\x6A\xC4\xDD\x48"
                             "\x58\x74\x80\xC6\xC5\x6E\x0E\x2B\xBE\x3B\xAD\x37\x40\x4C\x2C\x64"
                             "\x81\x1D\x2A\x7B\x14\xC6\x9C\xDB\x9B\xE2\xE1\x7C\x02\xCF\x3B\x4E"
                             "\xF2\x0D\x3A\x43\x81\xA7\xAF\xAD\xC8\x8B\x76\xDC\x01\x85\x74\x14"
                             "\xD6\xC0\xDA\xC0\x91\x3A\xB9\x98\x0D\xB9\x03\x06\xA7\x75\x24\x9D"
                             "\x4D\x5F\x1F\xFD\x7C\xBE\x0F\x01\xD8\x87\xD0\xFD\x66\x81\x5B\xB0"
                             "\x90\x43\x26\xE7\x9F\xC4\x67\xCC\xBD\xA3\x50\xD5\xC1\xC0\xE8\xA1"
                             "\x9A\xA7\x7B\xA5\x2B\xEC\x4E\xF7\x09\xDB\x14\x73\xAD\xF3\xAD\xE1"
                             "\x2B\x34\xF2\x38\x94\x4E\x02\x4C\xDE\x49\x0B\x0F\x64\xC5\x21\x05"
                             "\x0B\x7A\xBE\x47\x7A\x47\xA1\xE1\xBF\x4B\x00\xCC\x61\x24\x5F\x18"
                             "\xF0\x5F\x20\x00\x14\xD4\x33\x05\x79\x07\x7F\xF4\x11\x96\x83\x02"
                             "\x25\xBF\x43\xE0\x8A\x82\xDC\x7E\x63\xF9\x2C\xC6\xA8\xC0\x19\x03"
                             "\x71\x76\x7C\xFA\x11\xB6\x04\x28\x68\x67\x10\x29\xB9\xB3\x35\x00"
                             "\x74\xBD\x8C\x80\x42\xEB\xCE\x64\x7A\x47\x9F\x60\x11\x2B\x4A\x38"
                             "\x3A\x82\xDA\x79\xA9\x7B\x84\x67\xCF\x58\x74\xBD\x45\x28\x7D\x8E"
                             "\x0A\xB8\x59\x29\x50\xD5\xAC\xE3\x5D\xB6\x22\xEE\x36\x25\x42\xE7"
                             "\x0D\xC2\x1A\x03\xB5\xC3\x37\xE6\x82\x34\xF3\xD9\x24\xE2\x92\x88"
                             "\xC3\x17\x11\x17\x26\xD7\x57\xB6\x91\x86\xA2\x39\xD5\x3C\xCE\x39"
                             "\xE4\x45\x8C\x87\x47\x84\x62\xF2\x96\x40\x39\x4A\x3C\xE4\x70\x38"
                             "\x99\xB8\x9C\xE7\xA6\x21\x2A\x60\x39\x89\xF6\xBA\xB5\x3F\x9B\x45"
                             "\xE6\x76\x6D\x35\x81\xCF\x5D\xD9\x28\x2E\x09\x57\x67\x6D\x05\x04"
                             "\xC9\x52\xD5\x1A\x0E\xC0\x62\xC3\x65\x6E\xED\xD0\x30\x59\x81\x98"
                             "\x90\xAD\x6D\x72\x1B\xC8\x9F\x81\x88\xC9\xD2\xB6\x46\x17\xB0\x6B"
                             "\xDB\x2D\x8C\x65\x94\xBB\xED\xB5\x10\xBC\x9D\xB4\x0F\x8D\x94\xCE"
                             "\xF8\x76\x95\x44\x4C\x4E\xDA\x5B\x8E\x3D\xEE\x8C\x12\xA4\x34\xD5"
                             "\x49\x11\xE3\x28\x19\xB5\x76\x67\xB9\x18\x1C\xC1\xF9\x68\x7A\x8D"
                             "\x7B\x56\xB0\xBC\x3C\x87\x00\xFC\x74\xF8\xA1\xDB\xD7\x5A\x16\xCF"
                             "\x13\x0F\x4F\xF4\x94\xC3\xA5\x0A\xFB\xA6\xFB\x34\x3B\x1D\xCB\x54"
                             "\xF4\xEA\x5C\xFB\xAC\x4B\xD5\xF7\xE2\xE3\xDC\x48\x13\xD1\x93\xCB"
                             "\xF9\x7E\x7A\x19\xCB\xE4\xE3\x5A\xD1\x50\x6C\xDD\xFD\x9D\xB4\x87"
                             "\x87\x87\xC7\x29\x58\x06\x3D\xFC\xA0\x1E\xA9\xE7\x7F\xEB\xAC\x03"
                             "\x3D\x8E\xFA\xA6\xCB\x0F\xA5\x22\x8C\x91\x49\x5D\xBE\xB1\xAB\x25"
                             "\xCF\xD3\x54\xEC\x41\x92\xA6\xC9\x2D\xEB\xA7\x23\xE1\x2E\xFD\xAA"
                             "\xBE\x0E\x75\xC7\x1C\x0B\x36\xE0\x9A\x12\xCA\xA2\x1B\x32\x30\x19"
                             "\xA8\x09\x7B\xD8\x37\x45\xC9\x3D\x85\xF8\xB4\x84\x88\x3C\xA1\xF4"
                             "\x96\x0C\x30\xF5\x92\x97\x79\x5E\xCB\xCB\x37\xA5\xE9\x46\x92\x0A"
                             "\xDF\xC8\x6E\xCC\x60\x83\x10\x7B\xB5\x4D\x1B\xE1\x2D\x5A\x0F\x36"
                             "\xDA\xCC\x62\x5C\x27\x06\x2D\x7D\x66\x90\xBD\x30\x88\x63\xF3\x18"
                             "\xAF\x2F\x03\x2D\x31\x73\x27\x1B\xB4\x76\x3C\x61\x8A\x63\x4E\xD0"
                             "\x79\xFD\x26\x96\x85\x79\xC5\x95\x66\xB7\xCD\x44\x19\x51\x26\x20"
                             "\xDB\x12\x35\x8E\x7B\x02\xDB\xD4\xC4\x41\x37\x27\x90\xB0\xAE\xF3"
                             "\x3A\xB8\x5D\x88\xA2\xC4\xC4\xA7\x51\x14\x61\x51\x5B\x37\x8A\x48"
                             "\x92\xAC\xB0\x76\xA7\xD4\x3A\xAC\x05\x4C\x94\xA0\x66\xC5\x6B\x44"
                             "\x28\x9B\x14\xC4\x36\xC3\x0E\x4A\x4F\xD4\xBE\x7C\xCE\xA0\xEE\x4D"
                             "\x62\x46\x39\xD5\x58\xC6\x66\x44\xA4\xD8\x0F\xFF\x05\x45\x88\x8D"
                             "\x94\xF7\x9D\x63\x90\x33\x99\xE0\x10\xB8\x2A\xFC\xFE\x02\x03\xE1"
                             "\x2F\x84\x7C\xA1\x16\x94\xF2\x1C\xCB\x0B\x52\xC5\x41\x56\x08\xA3"
                             "\xE9\x7C\x07\x9D\xE3\xE6\xA5\x5C\xC5\x9F\x31\x18\x2A\x18\x25\x42"
                             "\x18\x22\x93\xCC\xE6\x02\x54\x72\xCC\x8E\x63\x5B\x44\x94\x52\x1C"
                             "\x0C\x40\x2F\x0D\x83\x14\x9E\x06\x8B\x62\x90\xBE\x52\xD0\xA9\xBE"
                             "\x75\x59\x3D\xC3\x3C\x8E\x8F\x41\x0D\x03\xB9\x10\xE2\xB1\xC6\xCA"
                             "\x3C\x15\x34\xA7\x24\x2C\x3F\x63\xB0\x72\xC2\x1A\xBD\xB4\x90\x0E"
                             "\xCA\xD1\x30\xC8\xD5\xB6\x42\x5E\xC4\xE2\xBA\xC4\xB2\xC1\xBE\xEF"
                             "\xFA\x23\x06\x2D\x27\xB4\x19\xB2\x96\x62\xF2\x82\x0C\x38\x69\x56"
                             "\x18\x1F\x3E\xE0\x02\x7C\x1D\x06\xDD\x2D\x90\x6E\xD1\x49\xC7\x5A"
                             "\x51\x18\xF1\x09\x83\x0B\xB3\xC9\x10\xF0\xE6\x35\x32\xE0\xDD\x62"
                             "\xF2\x22\x0E\x62\xBB\xBC\xD6\xC4\x57\x0C\xE6\x90\x84\x18\x27\x47"
                             "\x94\x15\xC8\x40\xEE\x83\x39\x66\x61\x87\xC9\x6D\x6E\x67\x87\x74"
                             "\xCB\x28\x7D\x29\xE9\x75\x21\xFA\x2D\x03\x93\x0C\xB5\x02\x53\x41"
                             "\x58\x59\xE2\x92\x8E\x01\x9E\x85\x3F\x9F\x30\x00\xE1\xC9\x6D\x2D"
                             "\x57\xE5\x18\x84\xF8\xC9\x07\xAE\xB9\x61\x40\x1D\x83\x9E\xB0\x62"
                             "\x2D\xCB\x82\x92\x69\xF8\x29\x83\x34\xFE\x31\x03\xB0\x97\x12\xF2"
                             "\x6A\xFA\x25\x03\x50\x2B\x89\xBA\xFE\x73\x19\xFC\x98\x01\xA8\x9B"
                             "\xE4\x88\x30\x7A\xCB\x80\x5D\x33\x60\xE6\x25\x4E\xAF\x65\xC0\x6E"
                             "\x24\x03\x79\x29\x0D\xEA\xAF\x18\xB0\xC2\xBE\xB4\x5E\xE5\xB7\xA0"
                             "\x5F\x61\xFD\x96\x01\x68\xE2\x4F\x19\x0C\xDC\x68\x62\x4E\xC4\xB3"
                             "\x5F\x7B\xC7\x40\xBE\x68\xE2\x07\x29\x97\xF9\x8A\x39\x8F\xE3\xD8"
                             "\xCD\x25\x68\x29\x28\xD4\x12\xEB\xE8\x38\x8D\x5F\x32\x20\xA4\x9E"
                             "\x0B\x81\xD6\x02\x3F\x3F\xC1\xC0\x20\x5B\x74\xFD\x96\x01\x18\x81"
                             "\x70\x5E\x96\x61\x8C\xC1\xDA\x50\x35\x64\x59\xAC\xBB\x6B\xA3\xA8"
                             "\x29\x45\xF5\x60\x32\xE5\x72\xD1\xB0\x53\x72\x9A\xE0\x16\x03\xB3"
                             "\xF5\x35\x03\xD4\xB0\x94\x1B\xA3\xA5\xCC\x37\x38\x36\x25\x6A\xA2"
                             "\x8F\xE3\x1B\x06\xF8\x45\x14\xE6\x24\xBD\x51\x4A\x78\x29\x51\xA4"
                             "\x7F\x25\x8A\x59\x1C\x05\xB2\x50\x07\x7B\x68\x3B\x52\x3E\x04\xAF"
                             "\x18\x2C\xEF\x18\x94\xDC\x5A\x3E\x8A\x0C\x82\x15\x67\xC1\xE2\x1E"
                             "\xC4\x0F\xB0\x87\x57\x0C\x16\xC5\x4D\xED\x97\x97\x60\xBA\x99\x7D"
                             "\x29\x7F\xFD\x79\xB4\x55\x15\x18\x4D\xD1\x57\x29\x68\x64\x4D\x2B"
                             "\x6C\x1A\x97\x12\x3D\x56\xC6\x1D\x17\xF6\x5A\x3C\xF6\x7F\x0D\x83"
                             "\xA9\xAA\x40\x42\x31\x0C\xEB\xAB\xA8\xEE\xAA\x0A\x2B\xFC\x35\xC3"
                             "\x59\xAA\x1E\x46\xD6\x4F\xFD\x93\x61\xF0\x54\xE1\xBF\x65\x2C\x5B"
                             "\xD5\xC3\x9C\x8F\xDB\xB1\x14\x0C\x7F\xAB\x0E\xF1\xF8\xF0\xD0\x1E"
                             "\xAE\x74\x68\xDB\xD1\x35\xB3\xEC\xD5\xF5\xA5\x6B\x1B\xBA\xD5\xD7"
                             "\xB7\x87\xB1\x75\x95\xD5\xEC\x79\x80\xBB\xEA\xF6\x98\x7E\x81\x97"
                             "\x6E\x1B\x64\x7A\x78\x78\x78\x78\x78\x78\x78\x78\x78\x78\x78\x78"
                             "\x78\xFC\x8F\xF1\x1F\xB4\x84\x64\xC3\x37\x9F\x8D\x16\x00\x00\x00"
                             "\x00\x49\x45\x4E\x44\xAE\x42\x60\x82",
                             (NVPair('Content-Type', 'application/octet-stream'),))


def view_page_by_title(test_script, page_name, space_key, with_editor=False, macro_list=[]):
    return view_page_by_page_url(test_script, "/display/%s/%s" % (space_key, url_encode(page_name)), with_editor, macro_list)


def view_page_by_page_url(test_script, page_url, with_editor=False, macro_list=[]):
    page_response = test_script.http("GET", page_url, {"showComments": "true"})
    if not page_response:
        return

    page_id = get_meta_attribute(page_response, 'ajs-page-id')
    space_key = get_meta_attribute(page_response, 'ajs-space-key')
    test_script.rest("GET", "/rest/watch-button/1.0/watchState/%s" % page_id)
    test_script.rest("GET", "/rest/likes/1.0/content/%s/likes" % page_id)
    test_script.rest("GET", "/rest/mywork/latest/status/notification/count?pageid=%s" % page_id)
    test_script.rest("GET", "/rest/inlinecomments/1.0/comments?containerId=%s" % page_id)
    test_script.rest("GET", "/rest/jiraanywhere/1.0/servers")
    test_script.rest("GET", "/rest/emoticons/1.0/")
    for macro in macro_list:
        macro_to_rests_map = macro_to_rests_mapping(test_script)
        if macro in macro_to_rests_map:
            grinder.logger.info("request for macro " + macro)
            test_script.rest("GET", macro_to_rests_map[macro](space_key, page_id))

    # editor loading ???
    grinder.logger.info("Loading editor for comment %s" % with_editor)
    if with_editor:
        space_key = get_meta_attribute(page_response, 'ajs-space-key')
        parent_page_id = get_meta_attribute(page_response, 'ajs-page-id')
        test_script.http("GET", "/plugins/editor-loader/editor.action?parentPageId=%s&pageId=%s&spaceKey=%s&atl_after_login_redirect=%s&timeout=12000"
                         % (parent_page_id, page_id, space_key, page_url))
    return page_response


def macro_browser_callback(cb_test_script, cb_page_id, cb_macro):
    editor_macro_review(cb_test_script, cb_page_id, cb_macro, '')
    editor_macro_placeholder(cb_test_script, cb_page_id, cb_macro, '')


def editor_macro_browser(test_script):
    test_script.http("GET", "/plugins/macrobrowser/browse-macros.action")


def editor_macro_review(test_script, content_id, macro_name, macro_body):
    macro_preview_form = {
        "contentId": content_id,
        "macro": {
            "name": macro_name,
            "body": macro_body,
            "params": {}
        }
    }
    test_script.rest("POST", "/rest/tinymce/1/macro/preview", macro_preview_form)
    test_script.rest("GET", "/plugins/macrobrowser/browse-macros-details.action?id=%s" % macro_name)


def editor_macro_placeholder(test_script, content_id, macro_name, macro_body):
    macro_preview_form = {
        "contentId": content_id,
        "macro": {
            "name": macro_name,
            "body": macro_body,
            "params": {}
        }
    }

    test_script.rest("POST", "/rest/tinymce/1/macro/placeholder", macro_preview_form)


def editor_save_draft(test_script, draft_id, page_id, content, page_title, space_key, sync_rev="dummy-sync-rev"):
    save_draft_from = {
        "draftId": draft_id,
        "pageId": page_id,
        "type": "page",
        "syncRev": sync_rev,
        "title": page_title,
        "spaceKey": space_key,
        "content": content
    }
    test_script.rest("POST", "/rest/tinymce/1/drafts", save_draft_from)


def editor_adding_page(test_script, parent_id, body, page_id, space_key, page_title, new_version, sync_rev):
    request_body = {"ancestors": [{"id": parent_id, "type": "page"}],
                    "body": {"editor": {"content": {"id": page_id},
                                        "representation": "editor",
                                        "value": body},
                             "id": page_id},
                    "id": page_id,
                    "space": {"key": space_key},
                    "status": "current",
                    "title": page_title,
                    "type": "page",
                    "version": {"message": "",
                                "minorEdit": "true",
                                "number": new_version,
                                "syncRev": sync_rev}}

    return test_script.rest("POST", "/rest/api/content", request_body)


def editor_publish_page(test_script,
                        parent_id,
                        body,
                        page_id,
                        space_key,
                        page_title,
                        sync_rev,
                        retry_count,
                        publish_delay=1000):
    """
    Method publishes a page
    :param test_script: reference to test script
    :param parent_id: parent page id
    :param body: page body
    :param page_id: page id
    :param space_key: space key
    :param page_title: page title
    :param sync_rev: sync revision
    :param retry_count: retry count in a case of error
    :param publish_delay: max delay before retry, ms
    :return: http response
    """
    response = ""
    while retry_count >= 0:
        retry_count -= 1
        new_version = str(int(get_page_version(test_script, page_id)) + 1)
        request_body = {"ancestors": [{"id": parent_id, "type": "page"}],
                        "body": {"editor": {"content": {"id": page_id},
                                            "representation": "editor",
                                            "value": body},
                                 "id": page_id},
                        "id": page_id,
                        "space": {"key": space_key},
                        "status": "current",
                        "title": page_title,
                        "type": "page",
                        "version": {"message": "",
                                    "minorEdit": "true",
                                    "number": new_version,
                                    "syncRev": sync_rev}}

        response = test_script.rest("PUT", "/rest/api/content/%s" % page_id, request_body)
        if is_http_ok():
            break
        time.sleep(random.randint(0, publish_delay) / 1000)
    return response


def put_comment(test_script, page_id, page_xsrf_token, html_comment):
    comment_form_data = {
        "atl_token": page_xsrf_token,
        "html": html_comment,
        "watch": "true"
    }

    test_script.http("POST", "/rest/tinymce/1/content/%s/comment?actions=true" % page_id,
                     comment_form_data, _no_check_header)


def _no_check_header(test_data, headers):
    headers.append(NVPair('Origin', test_data.base_url))
    headers.append(NVPair('X-Atlassian-Token', 'no-check'))


def get_page_version(test_script, page_id):
    response = test_script.rest("GET", "/rest/api/content/%s?expand=version" % page_id)
    return response["version"]["number"]


def get_instance_status(test_script):
    response = test_script.rest("GET", "/status")
    return response["state"]
