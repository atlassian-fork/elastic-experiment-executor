from java.util import HashMap, Random, Arrays
from java.lang import String, Integer
from urlparse import urlparse


def create_synchrony_options(synchrony_jwt_token, content_id, synchrony_application_id, synchrony_url_data):
    """
    Create and return synchrony interaction options as a map.
    :param synchrony_jwt_token: jwt token (from page source)
    :param content_id: content id (from page source)
    :param synchrony_application_id: synchrony client application id (from page source)
    :param synchrony_url_data: parsed synchrony service url (see ```parse_synchrony_url(url)``` for details)
    :return: interaction settings for synchrony
    """
    return _create_synchrony_settings(
        synchrony_jwt_token,
        "confluence-" + content_id,
        synchrony_application_id,
        synchrony_url_data["host"],
        synchrony_url_data["port"],
        synchrony_url_data["context_path"],
        synchrony_url_data["url"])


def _create_synchrony_settings(jwt_token, document_id, service_key, host, port, context_path, service_url):
    """
    Create and return synchrony interaction options as a map.
    :param jwt_token: jwt token (from page source)
    :param document_id: content id (from page source)
    :param service_key: synchrony client application id (from page source)
    :param host: synchrony host (or LB host for DC)
    :param port: synchrony port (or LB port for DC)
    :param context_path: synchrony context path
    :param service_url: synchrony service url
    :return: interaction settings for synchrony
    """
    options = HashMap()
    options.put("service-secret", "")
    options.put("path", "/sockjs/v1/websocket")
    options.put("id", Random().nextInt())
    options.put("duration", 500)
    options.put("run-sync", True)
    options.put("mut-config", _create_mutation_config())
    options.put("context-path", context_path)
    options.put("host", host)
    options.put("port", port)
    options.put("service-url", service_url)
    options.put("jwt-token", jwt_token)
    options.put("service-key", service_key)
    options.put("doc-id", document_id)
    return options


def _create_mutation_config():
    """
    Create and return synchrony interaction mutation options. It defines number and kind of operations
    :return: mutation operations config
    """
    config = HashMap()
    config.put("min-delay", 200)
    config.put("max-delay", 500)
    config.put("max-operations", 500)
    config.put("ops", Arrays.asList(
        Arrays.asList("insert-word", 1000),
        Arrays.asList("insert-element", 100),
        Arrays.asList("delete", 20),
        Arrays.asList("move", 10)
    ))
    return config


def prepare_synch_rev(sync_rev, id):
    """
    Create and return synchrony rev number based on existent one. As we are simulating synchrony client, we cannot get  
    real sync-rev, instead we're updating existent one (only last number (after the ".") is updated)
    :param sync_rev: sync_rev from page source 
    :param id: to be inserted after the last number in sync_rev (session revision)
    :return: updated sync_rev
    """
    if sync_rev == "dummy-sync-rev":
        return sync_rev
    revs = sync_rev.split(".")
    if len(revs) < 2:
        revs.append(String.valueOf(id))
    else:
        revs[len(revs) - 1] = String.valueOf(id)
    return ".".join(revs)


def parse_synchrony_url(url):
    """
    Parse synchrony url, to extract host, port and context path
    :param url: url to be parsed. It can be retrieved in a page "ajs-synchrony-base-url" meta tag
    :return: a map {
        "host":         host,
        "port":         port,
        "context_path": context_path,
        "url":          original_url
    } 
    """
    parsed_url = urlparse(url)
    netloc = parsed_url.netloc
    port = 80
    if parsed_url.scheme == "https":
        port = 443
    host = netloc
    colon_position = netloc.find(":")
    if colon_position != -1:
        port = Integer.valueOf(netloc[colon_position + 1:])
        host = netloc[:colon_position]
    return {
        "host": String.valueOf(host),
        "port": port,
        "context_path": String.valueOf(parsed_url.path),
        "url": String.valueOf(url)
    }


def validate_drafts_data(draft_id, draft_type, sync_rev):
    """
    Returns True if all the arguments are non empty strings, False otherwise
    :param draft_id: draft id (from the page meta)
    :param draft_type: draft type (from the page meta)
    :param sync_rev: synchrony revision (from the page meta)
    :return: True if all the arguments are non empty strings, False otherwise
    """
    if (not draft_id) or (not draft_type) or (not sync_rev):
        return False
    return True


def validate_synchrony_meta(synchrony_jwt_token, content_id, synchrony_application_id, synchrony_base_url):
    """
    Returns True if all the arguments are non empty strings, False otherwise
    :param synchrony_jwt_token: jwt token for Synchrony (from the page meta)
    :param content_id: content id (from the page meta)
    :param synchrony_application_id: synchrony client app id (from the page meta)
    :param synchrony_base_url: synchrony base url (from the page meta)
    :return: Returns True if all the arguments are non empty strings, False otherwise
    """
    if (not synchrony_jwt_token) or (not content_id) or (not synchrony_application_id) or (not synchrony_base_url):
        return False
    return True
