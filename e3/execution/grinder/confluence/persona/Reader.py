from confluence.common.helper.ConfluenceUserCreator import *
from confluence.common.helper.Authentication import login, logout
from confluence.common.helper.Utils import *
from confluence.common.CommonActions import *
from confluence.common.wrapper.User import User

from TestScript import TestScript

from net.grinder.script.Grinder import grinder

import re
import random


class Reader(TestScript):
    def __init__(self, number, args):
        super(Reader, self).__init__(number, args)
        self._current_user = None

        self._space_key = url_encode(get_space_key())
        self._pages_by_title = get_pages_by_title()
        self._space_actions = get_space_actions()
        self._pages_not_found = get_pages_not_found()
        self._rss_feeds = get_rss_feed_types()
        self._oc_pages = get_oc_pages()

    def __call__(self, *args, **kwargs):
        self._user_name = get_user_name()
        self._current_user = User(self._user_name, self._user_name)
        self._current_user.watch_space(self.test_data.base_url, self._space_key)

        grinder.logger.info("Starting Reader persona with user name %s" % self._user_name)
        safe_execute(self, self.logger, self._test, self.report_success)

    def _test(self):
        if self._current_user is None:
            return False

        login(self, self._current_user)

        visit_dashboard(self)

        run_num = grinder.getRunNumber()

        # view page
        item_num = random.randint(0, len(self._pages_by_title) - 1)
        # Load editor for comment for odd run number
        view_page_by_title(self, self._pages_by_title[item_num]["page"], self._space_key, with_editor=run_num % 2 is 0)
        is_page_by_title_ok = is_http_ok()

        # space action
        item_num = random.randint(0, len(self._space_actions) - 1)
        self.view_space_action(self._space_actions[item_num]["action"], self._space_key)
        is_view_space_action_ok = is_http_ok()

        # visit pages that doesn't exist
        item_num = random.randint(0, len(self._pages_not_found) - 1)
        view_page_by_title(self, self._pages_not_found[item_num]["page"], self._space_key)

        # view OC attachment (OC plugin should be enabled)
        item_num = random.randint(0, len(self._oc_pages) - 1)
        self.view_office_connector_page(self._space_key, self._oc_pages[item_num])
        is_view_office_connector_ok = is_http_ok()

        logout(self)
        return is_page_by_title_ok and is_view_space_action_ok and is_view_office_connector_ok

    def view_space_action(self, action, space_key):
        self.http("GET", "/%s" % action, {"key": space_key})

    def create_rss_feed(self, space_key, rss_type, content_type):
        title = "RSS Feed"
        self.http("GET", "/spaces/createrssfeed.action", {"spaces": space_key,
                                                          "sort": "modified",
                                                          "title": url_encode(title),
                                                          "maxResults": "30",
                                                          "publicFeed": "true",
                                                          "rssType": rss_type,
                                                          "types": content_type})

    def view_office_connector_page(self, space_key, params):
        page_name = params["page_name"]
        file_name = params["file_name"]

        # get page id
        page_response = view_page_by_title(self, page_name, space_key)
        page_id = get_meta_attribute(page_response, 'ajs-page-id')

        if ".doc" in file_name or ".xls" in file_name:
            self.http("GET", "/pages/worddav/preview.action", {"pageId": url_encode(page_id),
                                                               "fileName": url_encode(file_name)})
        else:
            # get attachment id
            pattern = 'data-attachment="%s" data-attachment-id="([0-9]+)"' % file_name
            match = re.search(pattern, page_response)
            attachment_id = match.group(1)

            self.http("GET", "/plugins/servlet/pptslide", {"attachmentId": url_encode(attachment_id),
                                                           "attachment": url_encode(file_name),
                                                           "pageId": url_encode(page_id)})