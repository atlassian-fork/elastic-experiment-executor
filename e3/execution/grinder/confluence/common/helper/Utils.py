from net.grinder.script.Grinder import grinder
from confluence.common.CommonActions import get_instance_status


def safe_execute(test_script, logger, test_callback, report_success):
    exception = None
    is_success = True

    try:
        instance_status = get_instance_status(test_script)
        if instance_status != "RUNNING":
            is_success = False
        else:
            is_success = test_callback()
    except Exception as ex:
        grinder.logger.error("Exception during execution test: %s" % ex)
        exception = ex

    # sleep a bit to give grinder process and thread a room to shutdown
    grinder.sleep(100)
    result = is_success and (exception is None)
    report_success(result)
    return result
