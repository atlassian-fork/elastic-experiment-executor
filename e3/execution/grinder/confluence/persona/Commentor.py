from confluence.common.helper.ConfluenceUserCreator import *
from confluence.common.helper.Authentication import login, logout
from confluence.common.helper.Utils import *
from confluence.common.CommonActions import *
from confluence.common.wrapper.User import User

from TestScript import TestScript

from net.grinder.script.Grinder import grinder


class Commentor(TestScript):

    def __init__(self, number, args):
        super(Commentor, self).__init__(number, args)
        self._current_user = None

        self._commented_page_list = get_pages_to_comment()
        self._comments = get_sample_comments()
        self._space_key = url_encode(get_space_key())

    def __call__(self, *args, **kwargs):
        self._user_name = get_user_name()
        self._current_user = User(self._user_name, self._user_name)

        grinder.logger.info("Starting Commentor persona with user name %s" % self._user_name)
        safe_execute(self, self.logger, self._test, self.report_success)

    def _test(self):
        run_num = grinder.getRunNumber()
        random_page_index = random.randint(0, len(self._commented_page_list) - 1)
        random_comment_index = random.randint(0, len(self._comments) - 1)
        page = self._commented_page_list[random_page_index]["page"]
        comment = self._comments[random_comment_index]
        grinder.logger.info("Page [%s] and comment %s" % (page, random_comment_index))

        login(self, self._current_user)

        page_response = view_page_by_title(self, page, self._space_key)
        if not is_http_ok():
            return False

        page_id = get_meta_attribute(page_response, 'ajs-page-id')
        space_key = get_meta_attribute(page_response, 'ajs-space-key')
        page_xsrf_token = get_meta_attribute(page_response, 'ajs-atl-token')

        if is_running_special_action(self, run_num):
            upload_image_into_page(self, page_id, space_key, page_xsrf_token)
            visit_page_attachments(self, page_id)

        grinder.logger.info("Page xsrf token [%s]" % page_xsrf_token)
        grinder.logger.info("Page id [%s]" % page_id)

        put_comment(self, page_id, page_xsrf_token, "<p>Comment from User %s</p>: %s" % (self._user_name, comment['value']))
        is_success = is_http_ok()
        view_page_by_title(self, page, self._space_key)
        logout(self)

        return is_success
